package gitlet;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.io.Serializable;
import java.util.HashSet;

/** The Commit Collection class that collects all commits.
 *  @author Yihe Li
 */
class CommitCollection implements Serializable {
    /** All commits by commit hashes. */
    private HashSet<String> _commits = new HashSet<String>();

    /** Dir for a this collection of all commits. */
    private static final String ALL_COMMIT_DIR = ".gitlet/.all_commit";

    /** Add COMMIT to collection. */
    static void addCommit(Commit commit) {
        File inFile = new File(ALL_COMMIT_DIR);
        CommitCollection collection = null;
        try {
            ObjectInputStream inp = new ObjectInputStream(new
                                        FileInputStream(inFile));
            collection = (CommitCollection) inp.readObject();
            inp.close();
            collection.insertCommit(commit);
        } catch (FileNotFoundException excp) {
            collection = new CommitCollection();
            collection.insertCommit(commit);
        } catch (IOException | ClassNotFoundException excp) {
            System.out.println("Exception adding head: " + excp);
        }
    }

    /** Helper to add COMMIT. */
    private void insertCommit(Commit commit) {
        try {
            File inFile = new File(ALL_COMMIT_DIR);
            _commits.add(commit.getSha1String());
            ObjectOutputStream out = new ObjectOutputStream(new
                                            FileOutputStream(inFile));
            out.writeObject(this);
            out.close();
        } catch (IOException excp) {
            System.out.println("Exception saving commit collection: " + excp);
        }
    }

    /** Return a copy of all commits. */
    static HashSet<String> getAllCommitsCopy() {
        File inFile = new File(ALL_COMMIT_DIR);
        CommitCollection collection = null;
        HashSet<String> copy = new HashSet<String>();
        try {
            ObjectInputStream inp = new ObjectInputStream(new
                                        FileInputStream(inFile));
            collection = (CommitCollection) inp.readObject();
            inp.close();
        } catch (IOException | ClassNotFoundException excp) {
            collection = null;
            System.out.println("Exception getting all commits copy :" + excp);
        }
        if (collection != null) {
            copy = new HashSet<String>(collection._commits);
        }
        return copy;
    }
}
