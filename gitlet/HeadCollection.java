package gitlet;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;

/** The Head Collection class that collects all heads.
 *  @author Yihe Li
 */
class HeadCollection implements Serializable {
    /** All heads from the branch name to commit hashes. */
    private HashMap<String, String> _heads = new HashMap<String, String>();

    /** Dir for a this collection of all commits. */
    private static final String ALL_HEAD_DIR = "/.all_heads";

    /** Add head with given BRANCH and COMMITHASH to HeadCollection. */
    static void addHead(String branch, String commitHash) {
        HeadCollection hc = retrieveFromFile();
        hc._heads.put(branch, commitHash);
        hc.save();
    }

    /** Return the head commit of a BRANCH. */
    static String getHeadCommitFromBranch(String branch) {
        return retrieveFromFile()._heads.get(branch);
    }

    /** Return the head commit of a BRANCH from a remote with REMOTEDIR. */
    static String getRemoteBranchHeadCommit(String branch, String remoteDir) {
        return retrieveFromFile(remoteDir)._heads.get(branch);
    }

    /** Return true if head collection contains BRANCH. */
    static boolean containsBranch(String branch) {
        return retrieveFromFile()._heads.containsKey(branch);
    }

    /** Remove branch with name BRANCH. Assuming the branch exists. */
    static void removeBranch(String branch) {
        HeadCollection hc = retrieveFromFile();
        hc._heads.remove(branch);
        hc.save();
    }

    /** Update head info for HEAD. */
    static void updateHead(Head head) {
        HeadCollection hc = retrieveFromFile();
        hc._heads.put(head.getBranchName(), head.getCommitHash());
        hc.save();
    }

    /** Update remote head in DIR to BRANCH and COMMIT. */
    static void updateRemoteHead(String dir, String branch, String commit) {
        HeadCollection hc = retrieveFromFile(dir);
        hc._heads.put(branch, commit);
        hc.save(dir);
        Head.updateRemoteHead(dir, branch, commit);
    }

    /** Return all heads copied. */
    static HashMap<String, String> getAllHeadsCopy() {
        return new HashMap<String, String>(retrieveFromFile()._heads);
    }

    /** Helper that returns the headCollection retrieved from file in DIR. */
    static HeadCollection retrieveFromFile() {
        return retrieveFromFile(".gitlet");
    }

    /** Helper that returns the headCollection retrieved from file in DIR. */
    static HeadCollection retrieveFromFile(String dir) {
        File inFile = new File(dir + ALL_HEAD_DIR);
        HeadCollection collection = null;
        try {
            ObjectInputStream inp = new ObjectInputStream(new
                                        FileInputStream(inFile));
            collection = (HeadCollection) inp.readObject();
            inp.close();
        } catch (FileNotFoundException excp) {
            collection = new HeadCollection();
        }  catch (IOException | ClassNotFoundException excp) {
            System.out.println("Exception adding head: " + excp);
            collection = null;
        }
        return collection;
    }

    /** Helper to save the headCollection to file. */
    private void save() {
        save(".gitlet");
    }

    /** Helper to save the headCollection to DIR. */
    private void save(String dir) {
        File inFile = new File(dir + ALL_HEAD_DIR);
        try {
            ObjectOutputStream out = new ObjectOutputStream(new
                                        FileOutputStream(inFile));
            out.writeObject(this);
            out.close();
        } catch (IOException excp) {
            System.out.println("Exception saving head collection: " + excp);
        }
    }
}
