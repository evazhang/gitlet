package gitlet;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.HashMap;
import java.io.File;
import java.io.IOException;


/** The suite of all JUnit tests for the Commit class.
 *  @author Yihe Li
 */
public class IndexTest {
    /** Test basic operations. */
    @Test
    public void testOperations() {
        try {
            File file = new File("file1");
            file.createNewFile();
            File file2 = new File("file2");
            file2.createNewFile();
            Index.trackFile("file1");
            assertTrue(Index.isFileTracked("file1"));
            Index.trackFile("file2");
            assertTrue(Index.isFileTracked("file2"));
            HashMap<String, String> expected = new HashMap<String, String>();
            expected.put("file1", fileHash("file1"));
            expected.put("file2", fileHash("file2"));
            assertEquals(expected, Index.getTrackedFiles());
            HashMap<String, String> files = new HashMap<String, String>();
            files.put("file1", fileHash("file2"));
            Index.trackFiles(files);
            expected.put("file1", fileHash("file2"));
            assertEquals(expected, Index.getTrackedFiles());
            expected.put("file1", fileHash("file1"));
            Index.updateTrackedFiles(expected);
            assertEquals(expected, Index.getTrackedFiles());
            Index.untrackFile("file1");
            assertFalse(Index.isFileTracked("file1"));
            Index.untrackFile("file2");
            assertFalse(Index.isFileTracked("file2"));
            file.delete();
            file2.delete();
        } catch (IOException excp) {
            excp.printStackTrace();
        }
    }

    /** Get file hash with FILENAME. */
    private String fileHash(String fileName) {
        byte[] fileByte = Utils.readContents(fileName);
        return Utils.sha1(fileByte);
    }
}
