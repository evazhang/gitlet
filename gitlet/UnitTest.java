package gitlet;

import ucb.junit.textui;
import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.*;
import java.io.File;

/** The suite of all JUnit tests for the gitlet package.
 *  @author
 */
public class UnitTest {

    /** Run the JUnit tests in the loa package. Add xxxTest.class entries to
     *  the arguments of runClasses to run other JUnit tests. */
    public static void main(String[] ignored) {
        textui.runClasses(CommitTest.class, HeadTest.class,
                          CommitCollectionTest.class, HeadCollectionTest.class,
                          IndexTest.class, UnitTest.class);
    }

    /** A dummy test to avoid complaint. */
    @Test
    public void placeholderTest() {
    }

    /** Clean up .gitlet after each testting. */
    @After
    public void cleanup() {
        Utils.deleteDir(new File(".gitlet"));
    }
}


