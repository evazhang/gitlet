package gitlet;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

/** The suite of all JUnit tests for the Head class.
 *  @author Yihe Li
 */
public class HeadTest {

    /** Head to test. */
    private Head testHead;

    /** Setup method. */
    @Before
    public void setup() {
        testHead = new Head("master", Utils.sha1("commit1"));
    }

    /** Test initialization. */
    @Test
    public void testSaveAndRetrieve() {
        testHead.save();
        Head retrieved = Head.retrieveHead();
        assertEquals(retrieved.getBranchName(), "master");
        assertEquals(retrieved.getCommitHash(), testHead.getCommitHash());
    }
}
