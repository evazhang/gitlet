package gitlet;

import java.io.File;
import java.util.HashSet;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.List;
import java.util.Map;

/** The logic class for Gitlet.
 *  @author Yihe Li
 */
public class Logic {

    /** Run init command. */
    static void initCommand() {
        if (isInitialized()) {
            System.out.println("A gitlet version-control system already "
                               + "exists in the current directory.");
            return;
        }
        File dir = new File(".gitlet");
        boolean successful = dir.mkdir();
        if (successful) {
            Commit initialCommit = new Commit(null, Utils.getCurrentTimeStamp(),
                                              INITIAL_COMMIT_MESSAGE,
                                              new HashMap<String, String>());
            String commitHash = initialCommit.getSha1String();
            Head head = new Head("master", commitHash);
            head.save();
            initialCommit.save();
            HeadCollection.addHead("master", commitHash);
        }
    }

    /** Run add command with argument FILENAME. */
    static void addCommand(String fileName) {
        if (!fileExists(fileName)) {
            System.out.println("File does not exist.");
            return;
        }
        Index.trackFile(fileName);
        File file = new File(fileName);
        String sha1 = Utils.getHashFromFileWithName(fileName);
        byte[] fileByte = Utils.readContents(file);
        Utils.writeContents(Utils.getFileFromHash(sha1), fileByte);
    }

    /** Run commit command with argument MESSAGE. */
    static void commitCommand(String message) {
        if (message.length() == 0) {
            System.out.println("Please enter a commit message.");
            return;
        }
        Head head = Head.retrieveHead();
        String headCommitHash = head.getCommitHash();
        String timeStamp = Utils.getCurrentTimeStamp();
        Commit nextCommit = new Commit(headCommitHash, timeStamp, message,
                                       Index.getTrackedFiles());
        if (headCommitHash != null) {
            Commit curCommit = Commit.retrieveCommitFromHash(headCommitHash);
            if (curCommit.hasSameTrackedFile(nextCommit)) {
                System.out.println("No changes added to the commit.");
                return;
            }
        }
        nextCommit.save();
        head.setCommitHash(nextCommit.getSha1String());
        head.save();
        HeadCollection.updateHead(head);
    }

    /** Run rm command with argument FILENAME. */
    static void rmCommand(String fileName) {
        String headCommitHash = Head.retrieveHead().getCommitHash();
        Commit curCommit = Commit.retrieveCommitFromHash(headCommitHash);
        if (!Index.isFileTracked(fileName) && !curCommit.
            hasTrackedFile(fileName)) {
            System.out.println("No reason to remove the file.");
            return;
        }
        String fileHash = Index.untrackFile(fileName);
        if (curCommit.hasTrackedFile(fileName)) {
            Utils.restrictedDelete(fileName);
        }
    }

    /** Run log command. */
    static void logCommand() {
        String headCommitHash = Head.retrieveHead().getCommitHash();
        while (headCommitHash != null) {
            Commit curCommit = Commit.retrieveCommitFromHash(headCommitHash);
            logCommit(curCommit);
            headCommitHash = curCommit.getParentCommitHash();
        }
    }

    /** Run global-log command. */
    static void globalLogCommand() {
        HashSet<String> allCommits = CommitCollection.getAllCommitsCopy();
        for (String commitHash : allCommits) {
            Commit commit = Commit.retrieveCommitFromHash(commitHash);
            logCommit(commit);
        }
    }

    /** Run find command with argument COMMITMSG. */
    static void findCommand(String commitMsg) {
        HashSet<String> allCommits = CommitCollection.getAllCommitsCopy();
        boolean foundOne = false;
        for (String commitHash : allCommits) {
            Commit commit = Commit.retrieveCommitFromHash(commitHash);
            if (commit.getMessage().compareTo(commitMsg) == 0) {
                foundOne = true;
                System.out.println(commit.getSha1String());
            }
        }
        if (!foundOne) {
            System.out.println("Found no commit with that message.");
        }
    }

    /** Run status command. */
    static void statusCommand() {
        HashMap<String, String> allHeads = HeadCollection.getAllHeadsCopy();
        Head head = Head.retrieveHead();
        String currentBranch = head.getBranchName();
        String headCommitHash = head.getCommitHash();
        System.out.println("=== Branches ===");
        for (String branch : new TreeSet<String>(allHeads.keySet())) {
            if (currentBranch.compareTo(branch) == 0) {
                System.out.println("*" + branch);
            } else {
                System.out.println(branch);
            }
        }
        List<String> plainFileNames = Utils.plainFilenamesIn(".");
        HashMap<String, String> index = Index.getTrackedFiles();
        TreeSet<String> stagedFiles =
            new TreeSet<String>(stagedFiles(index, headCommitHash).keySet());
        TreeSet<String> removedFiles =
            new TreeSet<String>(removedFiles(index, headCommitHash).keySet());
        TreeMap<String, String> modifiedFiles = new TreeMap<String, String>();
        TreeSet<String> untrackedFiles = new TreeSet<String>();

        for (String fileName : plainFileNames) {
            String fileHash = Utils.getHashFromFileWithName(fileName);
            if (!index.containsKey(fileName)) {
                untrackedFiles.add(fileName);
            } else {
                if (fileHash.compareTo(index.get(fileName)) != 0) {
                    modifiedFiles.put(fileName, "modified");
                }
                index.remove(fileName);
            }
        }

        for (String fileName : index.keySet()) {
            modifiedFiles.put(fileName, "deleted");
        }

        System.out.println("\n=== Staged Files ===");
        for (String file : stagedFiles) {
            System.out.println(file);
        }

        System.out.println("\n=== Removed Files ===");
        for (String file : removedFiles) {
            System.out.println(file);
        }

        System.out.println("\n=== Modifications Not Staged For Commit ===");
        for (Map.Entry<String, String> entry : modifiedFiles.entrySet()) {
            System.out.println(entry.getKey() + " (" + entry.getValue() + ")");
        }

        System.out.println("\n=== Untracked Files ===");
        for (String file : untrackedFiles) {
            System.out.println(file);
        }
    }

    /** Checkout file command with argument COMMITHASH, FILENAME. */
    static void checkoutFileCommand(String commitHash, String fileName) {
        if (commitHash == null) {
            Head head = Head.retrieveHead();
            commitHash = head.getCommitHash();
        }
        Commit dstCommit = Commit.retrieveCommitFromHash(commitHash);
        if (dstCommit == null) {
            System.out.println("No commit with that id exists.");
            return;
        }
        String fileHash = dstCommit.getFileHashFromFileName(fileName);
        if (fileHash == null) {
            System.out.println("File does not exist in that commit.");
            return;
        }
        Utils.copyFile(fileName, fileHash);
    }

    /** Checkout branch command with argument BRANCH. */
    static void checkoutBranchCommand(String branch) {
        String headCommitHash = HeadCollection.getHeadCommitFromBranch(branch);
        Head head = Head.retrieveHead();
        String currentBranch = head.getBranchName();
        if (!HeadCollection.containsBranch(branch)) {
            System.out.println("No such branch exists.");
            return;
        }
        if (branch.compareTo(currentBranch) == 0) {
            System.out.println("No need to checkout the current branch.");
            return;
        }
        checkoutCommitAndBranch(headCommitHash, branch, head);
    }

    /** Run branch command with argument BRANCH. */
    static void branchCommand(String branch) {
        Head head = Head.retrieveHead();
        if (HeadCollection.containsBranch(branch)) {
            System.out.println("A branch with that name already exists.");
            return;
        }
        HeadCollection.addHead(branch, head.getCommitHash());
    }

    /** Run rm-branch command with argument BRANCH. */
    static void rmBranchCommand(String branch) {
        Head head = Head.retrieveHead();
        if (!HeadCollection.containsBranch(branch)) {
            System.out.println("A branch with that name does not exist.");
            return;
        }
        if (head.getBranchName().compareTo(branch) == 0) {
            System.out.println("Cannot remove the current branch.");
            return;
        }
        HeadCollection.removeBranch(branch);
    }

    /** Run reset command with argument COMMITID. */
    static void resetCommand(String commitId) {
        Commit commit = Commit.retrieveCommitFromHash(commitId);
        if (commit == null) {
            System.out.println("No commit with that id exists.");
            return;
        }
        checkoutCommitAndBranch(commitId, null, Head.retrieveHead());
    }

    /** Run merge command with argument BRANCH. */
    static void mergeCommand(String branch) {
        Head head = Head.retrieveHead();
        String thisHash = head.getCommitHash();
        String thisBranch = head.getBranchName();
        HashMap<String, String> trackedFiles = Index.getTrackedFiles();
        if (!removedFiles(trackedFiles, thisHash).isEmpty()
            || !stagedFiles(trackedFiles, thisHash).isEmpty()) {
            System.out.println("You have uncommitted changes.");
            return;
        }
        String otherHash = HeadCollection.getHeadCommitFromBranch(branch);
        if (otherHash == null) {
            System.out.println("A branch with that name does not exist.");
            return;
        }
        if (branch.compareTo(thisBranch) == 0) {
            System.out.println("Cannot merge a branch with itself.");
            return;
        }
        Commit otherCommit = Commit.retrieveCommitFromHash(otherHash);
        Commit thisCommit = Commit.retrieveCommitFromHash(thisHash);
        String splitPointHash = thisCommit.lcaHash(otherCommit);
        if (splitPointHash.compareTo(otherHash) == 0) {
            System.out.println("Given branch is an ancestor of the current "
                               + "branch.");
            return;
        } else if (splitPointHash.compareTo(thisHash) == 0) {
            System.out.println("Current branch fast-forwarded.");
            return;
        }
        Commit splitPoint = Commit.retrieveCommitFromHash(splitPointHash);
        HashMap<String, String> filesInOtherCommit =
                                    otherCommit.getBlobMapCopy();
        HashMap<String, String> filesInSplitPoint =
                                    splitPoint.getBlobMapCopy();
        HashMap<String, String> modifiedFilesInOther =
                                    new HashMap<String, String>();
        HashMap<String, String> addedFilesInOther =
                                    new HashMap<String, String>();
        HashMap<String, String> removedFilesInOther =
                                    new HashMap<String, String>();
        for (Map.Entry<String, String> entry : filesInOtherCommit.entrySet()) {
            String fileName = entry.getKey();
            String newerHash = entry.getValue();
            if (filesInSplitPoint.containsKey(fileName)) {
                String olderHash = filesInSplitPoint.get(fileName);
                if (olderHash.compareTo(newerHash) != 0) {
                    modifiedFilesInOther.put(fileName, newerHash);
                }
                filesInSplitPoint.remove(fileName);
            } else {
                addedFilesInOther.put(fileName, newerHash);
            }
        }
        for (Map.Entry<String, String> entry : filesInSplitPoint.entrySet()) {
            removedFilesInOther.put(entry.getKey(), null);
        }

        HashSet<String> notModifiedFilesInThis =
                                    new HashSet<String>();
        HashMap<String, String> modifiedFilesInThis =
                                    new HashMap<String, String>();
        HashMap<String, String> addedFilesInThis =
                                    new HashMap<String, String>();
        HashMap<String, String> removedFilesInThis =
                                    new HashMap<String, String>();
        HashMap<String, String> filesInThisCommit = thisCommit.getBlobMapCopy();
        filesInSplitPoint = splitPoint.getBlobMapCopy();
        for (Map.Entry<String, String> entry : filesInThisCommit.entrySet()) {
            String fileName = entry.getKey();
            String newerHash = entry.getValue();
            if (filesInSplitPoint.containsKey(fileName)) {
                String olderHash = filesInSplitPoint.get(fileName);
                if (olderHash.compareTo(newerHash) == 0) {
                    notModifiedFilesInThis.add(fileName);
                } else {
                    modifiedFilesInThis.put(fileName, newerHash);
                }
                filesInSplitPoint.remove(fileName);
            } else {
                addedFilesInThis.put(fileName, newerHash);
            }
        }
        for (Map.Entry<String, String> entry : filesInSplitPoint.entrySet()) {
            removedFilesInThis.put(entry.getKey(), null);
        }

        boolean mergeSuccessful = true;
        HashMap<String, String> mergingFiles = new HashMap<String, String>();
        HashSet<String> removingFiles = new HashSet<String>();
        for (Map.Entry<String, String> entry : modifiedFilesInOther.
             entrySet()) {
            String fileName = entry.getKey();
            String fileHashOther = entry.getValue();
            if (notModifiedFilesInThis.contains(fileName)) {
                mergingFiles.put(fileName, fileHashOther);
            } else {
                if (modifiedFilesInThis.containsKey(fileName)) {
                    String fileHashThis = modifiedFilesInThis.get(fileName);
                    if (fileHashThis.compareTo(fileHashOther) == 0) {
                        mergingFiles.put(fileName, fileHashOther);
                    } else {
                        handleConflict(fileName, fileHashThis, fileHashOther);
                        mergeSuccessful = false;
                    }
                } else if (removedFilesInThis.containsKey(fileName)) {
                    String fileHashThis = removedFilesInThis.get(fileName);
                    handleConflict(fileName, fileHashThis, fileHashOther);
                    mergeSuccessful = false;
                }
            }
        }
        for (Map.Entry<String, String> entry : addedFilesInOther.entrySet()) {
            String fileName = entry.getKey();
            String fileHashOther = entry.getValue();
            if (!addedFilesInThis.containsKey(fileName)) {
                mergingFiles.put(fileName, fileHashOther);
            } else {
                String fileHashThis = addedFilesInThis.get(fileName);
                handleConflict(fileName, fileHashThis, fileHashOther);
                mergeSuccessful = false;
            }
        }
        for (Map.Entry<String, String> entry : removedFilesInOther.entrySet()) {
            String fileName = entry.getKey();
            String fileHashOther = entry.getValue();
            if (notModifiedFilesInThis.contains(fileName)) {
                removingFiles.add(fileName);
            } else if (modifiedFilesInThis.containsKey(fileName)) {
                String fileHashThis = modifiedFilesInThis.get(fileName);
                handleConflict(fileName, fileHashThis, fileHashOther);
                mergeSuccessful = false;
            }
        }

        HashSet<String> untrackedFiles = new HashSet<String>();
        List<String> plainFileNames = Utils.plainFilenamesIn(".");
        for (String fileName : plainFileNames) {
            if (!trackedFiles.containsKey(fileName)) {
                untrackedFiles.add(fileName);
            }
        }
        boolean mergingUntracked = false;
        for (Map.Entry<String, String> entry : mergingFiles.entrySet()) {
            if (untrackedFiles.contains(entry.getKey())) {
                mergingUntracked = true;
            }
        }
        for (String fileName : removingFiles) {
            if (untrackedFiles.contains(fileName)) {
                mergingUntracked = true;
            }
        }
        if (mergingUntracked) {
            System.out.println("There is an untracked file in the way; delete "
                               + "it or add it first.");
            return;
        }

        for (Map.Entry<String, String> entry : mergingFiles.entrySet()) {
            Utils.copyFile(entry.getKey(), entry.getValue());
        }
        Index.trackFiles(mergingFiles);
        for (String fileName : removingFiles) {
            Utils.restrictedDelete(fileName);
        }
        Index.untrackFiles(removingFiles);

        if (mergeSuccessful) {
            String message = "Merged " + thisBranch + " with " + branch + ".";
            Commit mergeCommit = new Commit(thisHash,
                                            Utils.getCurrentTimeStamp(),
                                            message, Index.getTrackedFiles());
            head.setCommitHash(mergeCommit.getSha1String());
            head.save();
            mergeCommit.save();
        } else {
            System.out.println("Encountered a merge conflict.");
        }
    }

    /** Run add-remote command with arguments NAME and DIR. */
    static void addRemoteCommand(String name, String dir) {
        Remote.addRemote(name, dir);
    }

    /** Run rm-remote command with argument NAME. */
    static void rmRemoteCommand(String name) {
        Remote.removeRemote(name);
    }

    /** Run push command with argument REMOTENAME, BRANCHNAME. */
    static void pushCommand(String remoteName, String branchName) {
        Head head = Head.retrieveHead();
        Remote.push(head, remoteName, branchName);
    }

    /** Run fetch command with argument REMOTENAME, BRANCHNAME. */
    static void fetchCommand(String remoteName, String branchName) {
        Remote.fetch(remoteName, branchName);
    }

    /** Run pull command with argument REMOTENAME, BRANCHNAME. */
    static void pullCommand(String remoteName, String branchName) {
        Remote.fetch(remoteName, branchName);
        mergeCommand(remoteName + "/" + branchName);
    }

    /** Exit if gitlet is not initialized. */
    static void exitIfGitletNotInitialized() {
        if (!isInitialized()) {
            System.out.println("Not in an initialized gitlet directory.");
            System.exit(0);
        }
    }

    /** Return true if gitlet is initialized. */
    private static boolean isInitialized() {
        return dirExists(".gitlet");
    }

    /** Return true if dir with DIRNAME exists. */
    private static boolean dirExists(String dirName) {
        File dir = new File(dirName);
        return dir.exists();
    }

    /** Return true if file with FILENAME exists. */
    private static boolean fileExists(String fileName) {
        return Utils.plainFilenamesIn(".").contains(fileName);
    }

    /** Log COMMIT. */
    private static void logCommit(Commit commit) {
        System.out.println("===");
        System.out.println("Commit " + commit.getSha1String());
        System.out.println(commit.getTimeStamp());
        System.out.println(commit.getMessage() + "\n");
    }

    /** Remove a file object with FILEHASH. */
    private static void removeFileObject(String fileHash) {
        File file = Utils.getFileFromHash(fileHash);
        file.delete();
    }

    /** Return the map of files of removed files. With TRACKEDFILES and
     *  last COMMITHASH. */
    private static HashMap<String, String> removedFiles(HashMap<String, String>
                                trackedfiles, String commitHash) {
        HashMap<String, String> removedFiles = new HashMap<String, String>();
        Commit curCommit = Commit.retrieveCommitFromHash(commitHash);
        HashMap<String, String> filesInCommit = curCommit.getBlobMapCopy();
        for (Map.Entry<String, String> entry : filesInCommit.entrySet()) {
            String fileName = entry.getKey();
            String fileHash = entry.getValue();
            if (!trackedfiles.containsKey(fileName)) {
                removedFiles.put(fileName, fileHash);
            }
        }
        return removedFiles;
    }

    /** Return the map of files of staged files. With TRACKEDFILES and
     *  last COMMITHASH. */
    private static HashMap<String, String> stagedFiles(HashMap<String, String>
                                trackedfiles, String commitHash) {
        HashMap<String, String> stagedFileHash = new HashMap<String, String>();
        Commit commit = Commit.retrieveCommitFromHash(commitHash);
        HashMap<String, String> filesInCommit = commit.getBlobMapCopy();
        for (Map.Entry<String, String> entry : trackedfiles.entrySet()) {
            String fileName = entry.getKey();
            String oldHash = filesInCommit.get(entry.getKey());
            String newHash = entry.getValue();
            if (oldHash == null) {
                stagedFileHash.put(fileName, newHash);
            } else if (oldHash != null && oldHash.compareTo(newHash) != 0) {
                stagedFileHash.put(fileName, newHash);
            }
        }
        return stagedFileHash;
    }

    /** Handle conflict when merging. Combine file from THISHASH and OTHERHASH
     *  and write into FILENAME*/
    private static void handleConflict(String fileName, String thisHash,
                                       String otherHash) {
        byte[] thisContent, otherContent;
        String thisStr, otherStr;
        if (thisHash == null) {
            thisStr = "";
        } else {
            thisContent = Utils.readContents(Utils.getFileFromHash(thisHash));
            thisStr = new String(thisContent);
        }
        if (otherHash == null) {
            otherStr = "";
        } else {
            otherContent = Utils.readContents(Utils.getFileFromHash(otherHash));
            otherStr = new String(otherContent);
        }
        String combined = "<<<<<<< HEAD\n" + thisStr + "=======\n" + otherStr
                            + ">>>>>>>\n";
        byte[] combinedByte = combined.getBytes();
        Utils.writeContents(new File(fileName), combinedByte);
    }

    /** From HEAD, Checkout a commit by COMMITHASH, BRANCH. BRANCH == null if
     *  not changing head branch. */
    private static void checkoutCommitAndBranch(String commitHash,
                                                String branch, Head head) {
        String currentCommitHash = head.getCommitHash();
        HashMap<String, String> trackedFiles = Index.getTrackedFiles();
        for (String fileName : Utils.plainFilenamesIn(".")) {
            if (!trackedFiles.containsKey(fileName)) {
                System.out.println("There is an untracked file in the way; "
                                   + "delete it or add it first.");
                return;
            }
        }
        for (String fileName : trackedFiles.keySet()) {
            Utils.restrictedDelete(fileName);
        }

        Commit headCommit = Commit.retrieveCommitFromHash(commitHash);
        HashMap<String, String> files = headCommit.getBlobMapCopy();
        for (Map.Entry<String, String> entry : files.entrySet()) {
            String fileName = entry.getKey();
            String fileHash = entry.getValue();
            Utils.copyFile(fileName, fileHash);
        }
        if (branch != null) {
            head.setBranchName(branch);
        }
        head.setCommitHash(commitHash);
        head.save();
        HeadCollection.updateHead(head);

        Index.updateTrackedFiles(files);
    }
    /** The message of the initial commit. */
    private static final String INITIAL_COMMIT_MESSAGE = "initial commit";
}
