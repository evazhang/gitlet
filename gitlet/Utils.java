package gitlet;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Formatter;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import static org.junit.Assert.*;


/** Assorted utilities.
 *  @author P. N. Hilfinger
 */
class Utils {

    /* SHA-1 HASH VALUES. */

    /** Returns the SHA-1 hash of the concatenation of VALS, which may
     *  be any mixture of byte arrays and Strings. */
    static String sha1(Object... vals) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            for (Object val : vals) {
                if (val instanceof byte[]) {
                    md.update((byte[]) val);
                } else if (val instanceof String) {
                    md.update(((String) val).getBytes(StandardCharsets.UTF_8));
                } else {
                    throw new IllegalArgumentException("improper type to sha1");
                }
            }
            Formatter result = new Formatter();
            for (byte b : md.digest()) {
                result.format("%02x", b);
            }
            return result.toString();
        } catch (NoSuchAlgorithmException excp) {
            throw new IllegalArgumentException("System does not support SHA-1");
        }
    }

    /** Returns the SHA-1 hash of the concatenation of the strings in
     *  VALS. */
    static String sha1(List<Object> vals) {
        return sha1(vals.toArray(new Object[vals.size()]));
    }

    /* FILE DELETION */

    /** Deletes FILE if it exists and is not a directory.  Returns true
     *  if FILE was deleted, and false otherwise.  Refuses to delete FILE
     *  and throws IllegalArgumentException unless the directory designated by
     *  FILE also contains a directory named .gitlet. */
    static boolean restrictedDelete(File file) {
        if (!(new File(file.getParentFile(), ".gitlet")).isDirectory()) {
            throw new IllegalArgumentException("not .gitlet working directory");
        }
        if (!file.isDirectory()) {
            return file.delete();
        } else {
            return false;
        }
    }

    /** Deletes the file named FILE if it exists and is not a directory.
     *  Returns true if FILE was deleted, and false otherwise.  Refuses
     *  to delete FILE and throws IllegalArgumentException unless the
     *  directory designated by FILE also contains a directory named .gitlet. */
    static boolean restrictedDelete(String file) {
        return restrictedDelete(new File(file));
    }

    /* READING AND WRITING FILE CONTENTS */

    /** Return the entire contents of FILE as a byte array.  FILE must
     *  be a normal file.  Throws IllegalArgumentException
     *  in case of problems. */
    static byte[] readContents(File file) {
        if (!file.isFile()) {
            throw new IllegalArgumentException("must be a normal file");
        }
        try {
            return Files.readAllBytes(file.toPath());
        } catch (IOException excp) {
            throw new IllegalArgumentException(excp.getMessage());
        }
    }

    /** Return the entire contents of file named NAME. */
    static byte[] readContents(String name) {
        return readContents(new File(name));
    }

    /** Write the entire contents of BYTES to FILE, creating or overwriting
     *  it as needed.  Throws IllegalArgumentException in case of problems. */
    static void writeContents(File file, byte[] bytes) {
        try {
            if (file.isDirectory()) {
                throw
                    new IllegalArgumentException("cannot overwrite directory");
            }
            Files.write(file.toPath(), bytes);
        } catch (IOException excp) {
            throw new IllegalArgumentException(excp.getMessage());
        }
    }

    /* DIRECTORIES */

    /** Filter out all but plain files. */
    private static final FilenameFilter PLAIN_FILES =
        new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return new File(dir, name).isFile();
            }
        };

    /** Returns a list of the names of all plain files in the directory DIR, in
     *  lexicographic order as Java Strings.  Returns null if DIR does
     *  not denote a directory. */
    static List<String> plainFilenamesIn(File dir) {
        String[] files = dir.list(PLAIN_FILES);
        if (files == null) {
            return null;
        } else {
            Arrays.sort(files);
            return Arrays.asList(files);
        }
    }

    /** Returns a list of the names of all plain files in the directory DIR, in
     *  lexicographic order as Java Strings.  Returns null if DIR does
     *  not denote a directory. */
    static List<String> plainFilenamesIn(String dir) {
        return plainFilenamesIn(new File(dir));
    }

    /* OTHER HELPERs */

    /** Return time stamp of current time. **/
    static String getCurrentTimeStamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    /** Return File from HASH. We might create dir that will be empty. */
    static File getFileFromHash(String hash) {
        assertNotNull(hash);
        String bucketName = hash.substring(0, 2);
        String fileName = hash.substring(2);
        File bucketDir = new File(OBJECT_DIR + bucketName);
        if (!bucketDir.exists()) {
            bucketDir.mkdirs();
        }
        if (hash.length() == SHA1_LENGTH) {
            return new File(OBJECT_DIR + bucketName + "/" + fileName);
        } else {
            for (String fileHash : Utils.plainFilenamesIn(bucketDir)) {
                if (fileHash.startsWith(fileName)) {
                    return new File(OBJECT_DIR + bucketName + "/" + fileHash);
                }
            }
            return new File(OBJECT_DIR + bucketName + "/" + fileName);
        }
    }

    /** Return remote File in REMOTEDIR from HASH. Creating new dirs. */
    static File getRemoteFileFromHash(String remoteDir, String hash) {
        assertNotNull(hash);
        String bucketName = hash.substring(0, 2);
        String fileName = hash.substring(2);
        File bucketDir = new File(remoteDir + "/objects/" + bucketName);
        if (!bucketDir.exists()) {
            bucketDir.mkdirs();
        }
        return new File(remoteDir + "/objects/" + bucketName + "/" + fileName);
    }

    /** Return Hash from with FILENAME. */
    static String getHashFromFileWithName(String fileName) {
        File targetFile = new File(fileName);
        byte[] fileByte = Utils.readContents(targetFile);
        return Utils.sha1(fileByte);
    }

    /** Dir name for git. */
    static final String GIT_DIR = ".gitlet/";

    /** Object directory. */
    static final String OBJECT_DIR = GIT_DIR + "objects/";

    /** Delete everything under a directory DIR. Return true if succeeded. */
    static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /** Write to file with FILENAME from file with FILEHASH. Overriding.*/
    static void copyFile(String fileName, String fileHash) {
        File file = Utils.getFileFromHash(fileHash);
        byte[] fileContent = Utils.readContents(file);
        File currentFile = new File(fileName);
        Utils.writeContents(currentFile, fileContent);
    }

    /** SHA1 length. */
    private static final int SHA1_LENGTH = 40;
}
