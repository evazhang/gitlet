package gitlet;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;
import java.util.HashMap;

/** The suite of all JUnit tests for the Commit class.
 *  @author Yihe Li
 */
public class CommitTest {

    /** Commit to test. */
    private Commit testCommit;
    @Before
    public void setup() {
        HashMap<String, String> blob = new HashMap<String, String>();
        blob.put("object1", Utils.sha1("Object1"));
        String timeStamp = Utils.getCurrentTimeStamp();
        String parentCommitHash = Utils.sha1("ParentCommit");
        String message = "Test";
        testCommit = new Commit(parentCommitHash, timeStamp, message,
                                       blob);
    }

    /** Test initialization. */
    @Test
    public void testInitialization() {
        String timeStamp = Utils.getCurrentTimeStamp();
        String parentCommitHash = Utils.sha1("ParentCommit");
        String message = "Test";
        assertEquals(testCommit.getTimeStamp(), timeStamp);
        assertEquals(testCommit.getMessage(), message);
        assertEquals(testCommit.getParentCommitHash(), parentCommitHash);
        assertEquals(testCommit.getSha1String(), Utils.sha1(parentCommitHash,
                     timeStamp, message));
    }

    /** Test comparing and contain file methods.
      * hasSameTrackedFile(Commit) & hasTrackedFile(String)*/
    @Test
    public void testComparingAndContaining() {
        assertTrue(testCommit.hasTrackedFile("object1"));
        assertFalse(testCommit.hasTrackedFile("otherObject"));

        HashMap<String, String> blob = new HashMap<String, String>();
        blob.put("object1", Utils.sha1("Object1"));
        String timeStamp = Utils.getCurrentTimeStamp();
        String parentCommitHash = Utils.sha1("ParentCommit");
        String message = "Test2";
        Commit testCommit2 = new Commit(parentCommitHash, timeStamp, message,
                                       blob);
        assertTrue(testCommit2.hasTrackedFile("object1"));
        assertFalse(testCommit2.hasTrackedFile("otherObject"));

        testCommit.hasSameTrackedFile(testCommit2);
    }

    /** Test if we get a COPY of blob.
     *  Test retrieving the hash of a file with file name. */
    @Test
    public void testGetBlobMapCopy() {
        HashMap<String, String> copy = testCommit.getBlobMapCopy();
        copy.put("object2", Utils.sha1("Object2"));
        assertFalse(testCommit.hasTrackedFile("object2"));
        assertEquals(testCommit.getFileHashFromFileName("object1"),
                     Utils.sha1("Object1"));
    }

    /** Test save and retrieve commit. */
    @Test
    public void testSaveAndRetrieve() {
        String hash = testCommit.getSha1String();
        testCommit.save();
        Commit retrieved = Commit.retrieveCommitFromHash(hash);
        assertEquals(testCommit.getMessage(), retrieved.getMessage());
        assertEquals(testCommit.getSha1String(), retrieved.getSha1String());
        assertEquals(testCommit.getTimeStamp(), retrieved.getTimeStamp());
        assertEquals(testCommit.getParentCommitHash(),
                     retrieved.getParentCommitHash());
        assertEquals(testCommit.getBlobMapCopy(), retrieved.getBlobMapCopy());

    }
}


