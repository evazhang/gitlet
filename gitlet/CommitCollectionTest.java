package gitlet;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.HashSet;


/** The suite of all JUnit tests for the Commit Collection class.
 *  @author Yihe Li
 */
public class CommitCollectionTest {

    /** Test initialization, adding, and getting copy*/
    @Test
    public void testBasic() {
        HashMap<String, String> blob = new HashMap<String, String>();
        blob.put("object1", Utils.sha1("Object1"));
        String timeStamp = Utils.getCurrentTimeStamp();
        String parentCommitHash = Utils.sha1("ParentCommit");
        String message = "Test";
        Commit testCommit = new Commit(parentCommitHash, timeStamp, message,
                                       blob);
        blob = new HashMap<String, String>();
        blob.put("object2", Utils.sha1("Object2"));
        timeStamp = Utils.getCurrentTimeStamp();
        parentCommitHash = Utils.sha1("ParentCommit");
        message = "Test2";
        Commit testCommit2 = new Commit(parentCommitHash, timeStamp, message,
                                       blob);
        HashSet<String> expected = new HashSet<String>();
        expected.add(testCommit.getSha1String());
        expected.add(testCommit2.getSha1String());
        CommitCollection.addCommit(testCommit);
        CommitCollection.addCommit(testCommit2);
        assertEquals(expected, CommitCollection.getAllCommitsCopy());

        expected.add(Utils.sha1("otherObject"));
        assertFalse(expected.equals(CommitCollection.getAllCommitsCopy()));
    }
}
