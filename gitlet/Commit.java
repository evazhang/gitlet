package gitlet;

import java.io.Serializable;
import java.util.HashMap;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.nio.file.Files;

/** The commit class for Gitlet.
 *  @author Yihe Li
 */
class Commit implements Serializable {
    /** Hash to parent commit hash. */
    private final String _parentCommitHash;
    /** Time stamp of the time when commit created. */
    private final String _timeStamp;
    /** Message associated with the commit. */
    private final String _message;
    /** Mapping for the tracked files from name to hash. */
    private final HashMap<String, String> _blobMap;
    /** SHA-1 hash for this commit. */
    private final String _sha1String;

    /** Constructor with PARENTCOMMITHASH, TIMESTAMP, MESSAGE, BLOBMAP. */
    Commit(String parentCommitHash, String timeStamp, String message,
           HashMap<String, String> blobMap) {
        _parentCommitHash = parentCommitHash;
        _timeStamp = timeStamp;
        _message = message;
        _blobMap = blobMap;
        if (parentCommitHash == null) {
            parentCommitHash = Utils.sha1("fakecommit");
        }
        _sha1String = Utils.sha1(parentCommitHash, timeStamp, message);
    }

    /** Return sha1. */
    public String getSha1String() {
        return _sha1String;
    }

    /** Return Time Stamp. */
    public String getTimeStamp() {
        return _timeStamp;
    }

    /** Return Message. */
    public String getMessage() {
        return _message;
    }

    /** Return parent commit hash. */
    public String getParentCommitHash() {
        return _parentCommitHash;
    }

    /** Compare blobMap with that of OTHERCOMMIT. Return true if they have same
     *  tracked files. */
    public boolean hasSameTrackedFile(Commit otherCommit) {
        return _blobMap.equals(otherCommit._blobMap);
    }

    /** Return if the commit tracked a file named FILENAME. */
    public boolean hasTrackedFile(String fileName) {
        return _blobMap.containsKey(fileName);
    }

    /** Return blob copy. */
    public HashMap<String, String> getBlobMapCopy() {
        return new HashMap<String, String>(_blobMap);
    }

    /** Return the hash of file with FILENAME. */
    public String getFileHashFromFileName(String fileName) {
        return _blobMap.get(fileName);
    }

    /** Return commit with given HASH retrieved from file,
     *  or null if no commit with that HASH exists. */
    static Commit retrieveCommitFromHash(String hash) {
        File file = Utils.getFileFromHash(hash);
        Commit commit = null;
        try {
            ObjectInputStream inp = new ObjectInputStream(new
                                        FileInputStream(file));
            commit = (Commit) inp.readObject();
            inp.close();
        } catch (IOException | ClassNotFoundException excp) {
            commit = null;
        }
        return commit;
    }

    /** Return commit from REMOTEDIR with given HASH retrieved from file,
     *  or null if no commit with that HASH exists. */
    static Commit retrieveRemoteCommitFromHash(String remoteDir, String hash) {
        File file = Utils.getRemoteFileFromHash(remoteDir, hash);
        Commit commit = null;
        try {
            ObjectInputStream inp = new ObjectInputStream(new
                                        FileInputStream(file));
            commit = (Commit) inp.readObject();
            inp.close();
        } catch (IOException | ClassNotFoundException excp) {
            commit = null;
        }
        return commit;
    }

    /** Save this commit to file and add to commit collection. */
    void save() {
        File file = Utils.getFileFromHash(_sha1String);
        try {
            ObjectOutputStream out = new ObjectOutputStream(new
                                        FileOutputStream(file));
            out.writeObject(this);
            out.close();
            CommitCollection.addCommit(this);
        } catch (IOException excp) {
            System.out.println("Save commit error: " + excp);
        }
    }

    /** Return parent commit. */
    Commit parentCommit() {
        return _parentCommitHash == null ?  null
                : Commit.retrieveCommitFromHash(_parentCommitHash);
    }

    /** Return parent commit in remote DIR. */
    Commit remoteParentCommit(String dir) {
        return _parentCommitHash == null ?  null
                : Commit.retrieveRemoteCommitFromHash(dir, _parentCommitHash);
    }

    /** Return true if this commit has OTHERCOMMIT as ancestor. */
    Boolean hasAncestor(Commit otherCommit) {
        String otherCommitHash = otherCommit.getSha1String();
        String thisCommitHash = _sha1String;
        while (thisCommitHash != null && thisCommitHash.compareTo(
               otherCommitHash) != 0) {
            thisCommitHash = parentHash(thisCommitHash);
        }
        return thisCommitHash != null;
    }

    /** Copy to REMOTEDIR. */
    void copyToRemote(String remoteDir) {
        try {
            for (String fileHash : _blobMap.values()) {
                File localFile = Utils.getFileFromHash(fileHash);
                File remoteFile = Utils.getRemoteFileFromHash(remoteDir,
                                                              fileHash);
                if (!remoteFile.exists()) {
                    Files.copy(localFile.toPath(), remoteFile.toPath());
                }
            }
            File localCommit = Utils.getFileFromHash(_sha1String);
            File remoteCommit = Utils.getRemoteFileFromHash(remoteDir,
                                                            _sha1String);
            if (!remoteCommit.exists()) {
                Files.copy(localCommit.toPath(), remoteCommit.toPath());
            }
        } catch (IOException excp) {
            excp.printStackTrace();
        }
    }

    /** Copy to local from REMOTEDIR. */
    void copyToLocal(String remoteDir) {
        try {
            for (String fileHash : _blobMap.values()) {
                File localFile = Utils.getRemoteFileFromHash(remoteDir,
                                                             fileHash);
                File remoteFile = Utils.getFileFromHash(fileHash);
                if (!remoteFile.exists()) {
                    Files.copy(localFile.toPath(), remoteFile.toPath());
                }
            }
            File localCommit = Utils.getRemoteFileFromHash(remoteDir,
                                                            _sha1String);
            File remoteCommit = Utils.getFileFromHash(_sha1String);
            if (!remoteCommit.exists()) {
                Files.copy(localCommit.toPath(), remoteCommit.toPath());
            }
        } catch (IOException excp) {
            excp.printStackTrace();
        }
    }

    /** Return the lowest common ancestor of this commit and OTHERCOMMIT. */
    String lcaHash(Commit otherCommit) {
        int thisHeight = getHeight();
        int otherHeight = otherCommit.getHeight();
        String upperCommitHash, lowerCommitHash;
        if (thisHeight < otherHeight) {
            upperCommitHash = _sha1String;
            lowerCommitHash = otherCommit._sha1String;
        } else {
            upperCommitHash = otherCommit._sha1String;
            lowerCommitHash = _sha1String;
        }
        int dh = Math.abs(thisHeight - otherHeight);
        for (int i = 0; i < dh; i++) {
            lowerCommitHash = parentHash(lowerCommitHash);
        }
        while (lowerCommitHash != null && upperCommitHash != null) {
            if (lowerCommitHash.compareTo(upperCommitHash) == 0) {
                return lowerCommitHash;
            }
            lowerCommitHash = parentHash(lowerCommitHash);
            upperCommitHash = parentHash(upperCommitHash);
        }
        return null;
    }

    /** Return the parent hash from COMMITHASH. */
    private static String parentHash(String commitHash) {
        Commit commit = Commit.retrieveCommitFromHash(commitHash);
        return commit._parentCommitHash;
    }

    /** Return the depth of a commit .*/
    private int getHeight() {
        String commitHash = _parentCommitHash;
        int height = 1;
        while (commitHash != null) {
            Commit commit = Commit.retrieveCommitFromHash(commitHash);
            height += 1;
            commitHash = commit._parentCommitHash;
        }
        return height;
    }

}
