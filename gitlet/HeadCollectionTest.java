package gitlet;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.HashMap;

/** The suite of all JUnit tests for the Head Collection class.
 *  @author Yihe Li
 */
public class HeadCollectionTest {

    /** Test initialization, adding, and getting copy*/
    @Test
    public void testBasic() {
        HashMap<String, String> blob = new HashMap<String, String>();
        blob.put("object1", Utils.sha1("Object1"));
        String timeStamp = Utils.getCurrentTimeStamp();
        String parentCommitHash = Utils.sha1("ParentCommit");
        String message = "Test";
        Commit testCommit = new Commit(parentCommitHash, timeStamp, message,
                                       blob);
        blob = new HashMap<String, String>();
        blob.put("object2", Utils.sha1("Object2"));
        timeStamp = Utils.getCurrentTimeStamp();
        parentCommitHash = Utils.sha1("ParentCommit");
        message = "Test2";
        Commit testCommit2 = new Commit(parentCommitHash, timeStamp, message,
                                       blob);
        HashMap<String, String> expected = new HashMap<String, String>();
        expected.put("master", testCommit.getSha1String());
        expected.put("test", testCommit2.getSha1String());
        HeadCollection.addHead("master", testCommit.getSha1String());
        HeadCollection.addHead("test", testCommit2.getSha1String());
        assertEquals(expected, HeadCollection.getAllHeadsCopy());
        expected.put("otherBranch", Utils.sha1("otherObject"));
        assertFalse(expected.equals(HeadCollection.getAllHeadsCopy()));


        assertEquals(testCommit.getSha1String(),
                     HeadCollection.getHeadCommitFromBranch("master"));
        assertEquals(testCommit2.getSha1String(),
                     HeadCollection.getHeadCommitFromBranch("test"));
        assertTrue(HeadCollection.containsBranch("master"));
        assertFalse(HeadCollection.containsBranch("otherBranch"));
        assertNull(HeadCollection.getHeadCommitFromBranch("otherBranch"));

        HeadCollection.removeBranch("test");
        assertFalse(HeadCollection.containsBranch("test"));

        Head head = new Head("master", testCommit2.getSha1String());
        HeadCollection.updateHead(head);
        assertEquals(testCommit2.getSha1String(),
                     HeadCollection.getHeadCommitFromBranch("master"));
    }
}
