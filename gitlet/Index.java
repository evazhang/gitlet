package gitlet;

import java.io.Serializable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

/** The index file class for Gitlet.
 *  @author Yihe Li
 */
class Index implements Serializable {

    /** Mapping from file name to file sha1 hash. */
    private HashMap<String, String> _blobMap =
                        new HashMap<String, String>();

    /** Dir for index file. */
    private static final String INDEX_DIR = ".gitlet/.index";

    /** Track a file with FILENAME. */
    static void trackFile(String fileName) {
        Index index = retrieveIndex();
        index.addFile(fileName);
        index.save();
    }

    /** Track a files in FILES. */
    static void trackFiles(Map<String, String> files) {
        Index index = retrieveIndex();
        for (Map.Entry<String, String> entry : files.entrySet()) {
            index._blobMap.put(entry.getKey(), entry.getValue());
        }
        index.save();
    }

    /** Untrack a file with FILENAME. Return the fileHash of that file. */
    static String untrackFile(String fileName) {
        Index index = retrieveIndex();
        String fileHash = index._blobMap.get(fileName);
        index.removeFile(fileName);
        index.save();
        return fileHash;
    }

    /** Untrack FILES. */
    static void untrackFiles(Set<String> files) {
        Index index = retrieveIndex();
        for (String fileName : files) {
            index.removeFile(fileName);
        }
        index.save();
    }

    /** Return true if a file with FILENAME is tracked. */
    static boolean isFileTracked(String fileName) {
        return retrieveIndex().hasStagedFile(fileName);
    }

    /** Update the tracking files with FILE. */
    static void updateTrackedFiles(HashMap<String, String> file) {
        Index index = retrieveIndex();
        index._blobMap = new HashMap<String, String>(file);
        index.save();
    }

    /** Return a mapping of all the files tracked. */
    static HashMap<String, String> getTrackedFiles() {
        return retrieveIndex().getBlobMapCopy();
    }

    /** PRIVATE HELPER */

    /** Add mapping with given file named FILENAME. **/
    private void addFile(String fileName) {
        byte[] fileByte = Utils.readContents(fileName);
        String newHash = Utils.sha1(fileByte);
        _blobMap.put(fileName, newHash);
    }

    /** Return a copy of blobMap. */
    private HashMap<String, String> getBlobMapCopy() {
        return new HashMap<String, String>(_blobMap);
    }

    /** Remove a new file with FILENAME. */
    private void removeFile(String fileName) {
        _blobMap.remove(fileName);
    }

    /** Return true if file named FILENAME in blob map. */
    private boolean hasStagedFile(String fileName) {
        return _blobMap.containsKey(fileName);
    }

    /** Return the indexFile retrieved from file. */
    private static Index retrieveIndex() {
        File inFile = new File(INDEX_DIR);
        Index indexFile = null;
        try {
            ObjectInputStream inp = new ObjectInputStream(new
                                        FileInputStream(inFile));
            indexFile = (Index) inp.readObject();
            inp.close();
        } catch (FileNotFoundException excp) {
            indexFile = new Index();
        } catch (IOException | ClassNotFoundException excp) {
            indexFile = null;
        }
        return indexFile;
    }

    /** Save the indexfile to file. */
    private void save() {
        File inFile = new File(INDEX_DIR);
        try {
            ObjectOutputStream out = new ObjectOutputStream(new
                                        FileOutputStream(inFile));
            out.writeObject(this);
            out.close();
        } catch (IOException excp) {
            excp.printStackTrace();
        }
    }
}
