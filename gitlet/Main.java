package gitlet;

/** Driver class for Gitlet, the tiny stupid version-control system.
 *  @author Yihe Li
 */
public class Main {

    /** Usage: java gitlet.Main ARGS, where ARGS contains
     *  <COMMAND> <OPERAND> .... */
    public static void main(String... args) {
        if (args.length == 0) {
            System.out.println("Please enter a command.");
            System.exit(0);
        }
        String command = args[0];
        String operand = args.length > 1 ? args[1] : null;
        checkIncorrectOperands(command, operand);
        executeCommand(command, operand, args);
    }

    /** Execute COMMAND with OPERAND and ARGS. */
    private static void executeCommand(String command, String operand,
                                       String[] args) {
        switch (command) {
        case "init":
            Logic.initCommand();
            break;
        case "add":
            Logic.exitIfGitletNotInitialized();
            Logic.addCommand(operand);
            break;
        case "commit":
            Logic.exitIfGitletNotInitialized();
            Logic.commitCommand(operand);
            break;
        case "checkout":
            Logic.exitIfGitletNotInitialized();
            if (args.length == 2) {
                Logic.checkoutBranchCommand(args[1]);
            } else if (args.length == 3 && args[1].compareTo("--") == 0) {
                Logic.checkoutFileCommand(null, args[2]);
            } else if (args.length == 4 && args[2].compareTo("--") == 0) {
                Logic.checkoutFileCommand(args[1], args[3]);
            } else {
                exitWithIncorrectOperands();
            }
            break;
        case "add-remote":
            Logic.exitIfGitletNotInitialized();
            if (args.length != 3 || args[2].isEmpty()
                || !args[2].endsWith("/.gitlet")) {
                exitWithIncorrectOperands();
            }
            Logic.addRemoteCommand(args[1], args[2]);
            break;
        case "push":
            Logic.exitIfGitletNotInitialized();
            if (args.length != 3) {
                exitWithIncorrectOperands();
            }
            Logic.pushCommand(args[1], args[2]);
            break;
        case "fetch":
            Logic.exitIfGitletNotInitialized();
            if (args.length != 3) {
                exitWithIncorrectOperands();
            }
            Logic.fetchCommand(args[1], args[2]);
            break;
        case "pull":
            Logic.exitIfGitletNotInitialized();
            if (args.length != 3) {
                exitWithIncorrectOperands();
            }
            Logic.pullCommand(args[1], args[2]);
            break;
        default:
            executeOtherCommand(command, operand);
        }
    }

    /** Execute other COMMAND with OPERAND to avoid style error. */
    private static void executeOtherCommand(String command, String operand) {
        switch (command) {
        case "rm":
            Logic.exitIfGitletNotInitialized();
            Logic.rmCommand(operand);
            break;
        case "log":
            Logic.exitIfGitletNotInitialized();
            Logic.logCommand();
            break;
        case "global-log":
            Logic.exitIfGitletNotInitialized();
            Logic.globalLogCommand();
            break;
        case "find":
            Logic.exitIfGitletNotInitialized();
            Logic.findCommand(operand);
            break;
        case "status":
            Logic.exitIfGitletNotInitialized();
            Logic.statusCommand();
            break;
        case "branch":
            Logic.exitIfGitletNotInitialized();
            Logic.branchCommand(operand);
            break;
        case "rm-branch":
            Logic.exitIfGitletNotInitialized();
            Logic.rmBranchCommand(operand);
            break;
        case "reset":
            Logic.exitIfGitletNotInitialized();
            Logic.resetCommand(operand);
            break;
        case "merge":
            Logic.exitIfGitletNotInitialized();
            Logic.mergeCommand(operand);
            break;
        case "rm-remote":
            Logic.exitIfGitletNotInitialized();
            Logic.rmRemoteCommand(operand);
            break;
        default:
            System.out.println("No command with that name exists.");
            System.exit(0);
        }
    }

    /** Check if there is incorrect operands error with COMMAND, OPERAND. */
    private static void checkIncorrectOperands(String command, String operand) {
        switch (command) {
        case "add": case "commit": case "rm": case "find": case "branch":
        case "rm-branch": case "reset": case "merge": case "rm-remote":
            if (operand == null) {
                exitWithIncorrectOperands();
            }
            break;
        case "init": case "log": case "global-log": case "status":
            if (operand != null) {
                exitWithIncorrectOperands();
            }
            break;
        default:
        }
    }

    /** Exit the program when incorrect operands. */
    private static void exitWithIncorrectOperands() {
        System.out.println("Incorrect operands. ");
        System.exit(0);
    }
}
