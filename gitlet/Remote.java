package gitlet;

import java.io.Serializable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.util.HashMap;

/** The remote file class for Gitlet.
 *  @author Yihe Li
 */
class Remote implements Serializable {

    /** Mapping from file name to file sha1 hash. */
    private HashMap<String, String> _remotes =
                        new HashMap<String, String>();

    /** Dir for index file. */
    private static final String REMOTE_DIR = ".gitlet/.remote";

    /** Add a remote with REMOTENAME and REMOTEDIR. */
    static void addRemote(String remoteName, String remoteDir) {
        Remote index = retrieveRemote();
        if (index._remotes.containsKey(remoteName)) {
            System.out.println("A remote with that name already exists.");
            return;
        }
        index._remotes.put(remoteName, remoteDir);
        index.save();
    }

    /** Remove a remote with REMOTENAME. */
    static void removeRemote(String remoteName) {
        Remote index = retrieveRemote();
        if (!index._remotes.containsKey(remoteName)) {
            System.out.println("A remote with that name does not exist.");
            return;
        }
        index._remotes.remove(remoteName);
        index.save();
    }

    /** Push LOCALHEAD to the remote head designated by REMOTENAME and
     *  BRANCHNAME. */
    static void push(Head localHead, String remoteName, String branchName) {
        Remote remote = retrieveRemote();
        String remoteDirStr = remote._remotes.get(remoteName);
        File remoteDir = new File(remoteDirStr);
        if (!remoteDir.exists()) {
            System.out.println("Remote directory not found.");
            return;
        }
        String remoteCommitHash = HeadCollection.getRemoteBranchHeadCommit(
                                            branchName, remoteDirStr);
        Commit remoteCommit = Commit.retrieveRemoteCommitFromHash(remoteDirStr,
                                                          remoteCommitHash);
        Commit localCommit = Commit.retrieveCommitFromHash(localHead.
                                                          getCommitHash());
        if (!localCommit.hasAncestor(remoteCommit)) {
            System.out.println("Please pull down remote changes before "
                               + "pushing.");
            return;
        }
        while (localCommit.getSha1String().compareTo(remoteCommitHash) != 0) {
            localCommit.copyToRemote(remoteDirStr);
            localCommit = localCommit.parentCommit();
        }
        HeadCollection.updateRemoteHead(remoteDirStr,
                        localHead.getBranchName(), localHead.getCommitHash());
    }

    /** Fetch the remote head designated by REMOTENAME and
     *  BRANCHNAME. */
    static void fetch(String remoteName, String branchName) {
        Remote remote = retrieveRemote();
        String remoteDirStr = remote._remotes.get(remoteName);
        File remoteDir = new File(remoteDirStr);
        if (!remoteDir.exists()) {
            System.out.println("Remote directory not found.");
            return;
        }
        String commitHash = HeadCollection.getRemoteBranchHeadCommit(
                                            branchName, remoteDirStr);
        if (commitHash == null) {
            System.out.println(" That remote does not have that branch.");
            return;
        }
        Commit remoteCommit = Commit.retrieveRemoteCommitFromHash(remoteDirStr,
                                                                  commitHash);
        HeadCollection.addHead(remoteName + "/" + branchName, commitHash);
        while (remoteCommit != null) {
            remoteCommit.copyToLocal(remoteDirStr);
            remoteCommit = remoteCommit.remoteParentCommit(remoteDirStr);
        }
    }

    /** Return the remote file retrieved from file. */
    static Remote retrieveRemote() {
        File inFile = new File(REMOTE_DIR);
        Remote indexFile = null;
        try {
            ObjectInputStream inp = new ObjectInputStream(new
                                        FileInputStream(inFile));
            indexFile = (Remote) inp.readObject();
            inp.close();
        } catch (FileNotFoundException excp) {
            indexFile = new Remote();
        } catch (IOException | ClassNotFoundException excp) {
            indexFile = null;
        }
        return indexFile;
    }

    /** Save the indexfile to file. */
    private void save() {
        File inFile = new File(REMOTE_DIR);
        try {
            ObjectOutputStream out = new ObjectOutputStream(new
                                        FileOutputStream(inFile));
            out.writeObject(this);
            out.close();
        } catch (IOException excp) {
            excp.printStackTrace();
        }
    }
}
