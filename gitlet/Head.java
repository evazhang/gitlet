package gitlet;

import java.io.Serializable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;

/** The Head class that contains the head of commit tree.
 *  @author Yihe Li
 */
class Head implements Serializable {
    /** Name of the branch. */
    private String _branchName;

    /** Commit hahs. */
    private String _commitHash;

    /** Dir of head file. */
    private static final String HEAD_DIR = "/.head";

    /** Constructor with BRANCH and COMMITHASH. */
    Head(String branch, String commitHash) {
        _branchName = branch;
        _commitHash = commitHash;
    }

    /** Constructor. */
    Head() {

    }

    /** Set branch name to BRANCH. */
    void setBranchName(String branch) {
        _branchName = branch;
    }

    /** Set commit hash to COMMITHASH. */
    void setCommitHash(String commitHash) {
        _commitHash = commitHash;
    }

    /** Return branch name. */
    String getBranchName() {
        return _branchName;
    }

    /** Return commit hash. */
    String getCommitHash() {
        return _commitHash;
    }

    /** Update remote head in DIR to BRANCH and COMMIT. */
    static void updateRemoteHead(String dir, String branch, String commit) {
        Head head = Head.retrieveHead(dir);
        if (head._branchName.compareTo(branch) != 0) {
            return;
        }
        head._commitHash = commit;
        head.save(dir);
    }

    /** Return head retrieved from file. */
    static Head retrieveHead() {
        return retrieveHead(".gitlet");
    }

    /** Return the head retrieved from DIR. */
    static Head retrieveHead(String dir) {
        File inFile = new File(dir + HEAD_DIR);
        Head head = null;
        try {
            ObjectInputStream inp = new ObjectInputStream(new
                                        FileInputStream(inFile));
            head = (Head) inp.readObject();
            inp.close();
        } catch (IOException | ClassNotFoundException excp) {
            head = null;
        }
        return head;
    }

    /** Save head to file. */
    void save() {
        save(".gitlet");
    }

    /** Save head to DIR. */
    void save(String dir) {
        File inFile = new File(dir + HEAD_DIR);
        try {
            ObjectOutputStream out = new ObjectOutputStream(new
                                        FileOutputStream(inFile));
            out.writeObject(this);
            out.close();
        } catch (IOException excp) {
            excp.printStackTrace();
        }
    }
}
